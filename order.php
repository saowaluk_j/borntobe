<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BORN TO BE CAP</title>
	<link rel="stylesheet" type="text/css" href="assets/css/cssPc.css">
</head>
<body>
<div class="text-right" id="login">
	ชื่อผู้ใช้: <input type="text">
	รหัสผ่าน: <input type="password">
	<button type="button" onclick="window.location.href='manage.html'">เข้าสู่ระบบ</button>
</div>
<header>
	<h1 class="no-span">BORN TO BE CAP</h1>
	<ul class="list-inline">
		<li id="thisPage"><a href="home.php">หน้าแรก</a></li>
		<li class="list-hover"><a href="product.php">สินค้า</a></li>
		<li class="list-hover"><a href="order.php">การสั่งซื้อ</a></li>
		<li class="list-hover"><a href="contact.php">ติดต่อเรา</a></li>
	</ul>
</header>
<section>
	<div class="container">
		<div class="row">
			<h3>ยืนยันการสั่งซื้อ</h3>
		</div>
		<div class="row">
			<br><br><br><br>
			<h1 class="text-center"> รายการสั่งซื้อ </h1>
			<br><br><br><br>

		</div>
		<div class="row">
			<h3 class="text-right">ราคารวม : <span class ="txtred">  บาท</span></h3>
		</div>
		<div class="row">
			<hr>
			<button class ="btn" style="float: left">ไปหน้าสินค้า</button>
			<button class ="btn" style="float: right">ยืนยัน</button>
			<br>
		</div>
	</div>
</section>
<footer>
	<div class="row">
		<div class="col-xs-4">
			<p> แนะนำการสั่งซื้อ </p>
			<p> สินค้าทั้งหมด </p>
			<p> เกี่ยวกับเรา </p>
			<p> ติดต่อเรา </p>
			<p> ช่องทางการติดต่อ </p>
			<img class="social" src="<?php echo asset('assets/img/twitter-icon.png'); ?>">
			<img class="social" src="<?php echo asset('assets/img/social-facebook-box-blue-icon.png'); ?>">
			<img class="social" src="<?php echo asset('assets/img/Active-Instagram-3-icon.png'); ?>">

		</div>
		<div class="col-xs-4">

		</div>
		<div class="col-xs-4">

		</div>
	</div>
	<div class="row">
		<div class="text-center">ผลิตโดย born to be cap group สำหรับ CS387 ©2015 มหาวิทยาลัยธรรมศาสตร์</div>
	</div>
</footer>
<script src="home.js"></script>
</body>
</html>
