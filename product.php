<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BORN TO BE CAP</title>
	<link rel="stylesheet" type="text/css" href="assets/css/cssPc.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="text-right" id="login">
	ชื่อผู้ใช้: <input type="text">
	รหัสผ่าน: <input type="password">
	<button type="button" onclick="window.location.href='manage.html'">เข้าสู่ระบบ</button>
</div>
<header>
	<h1 class="no-span">BORN TO BE CAP</h1>
	<ul class="list-inline">
		<li id="thisPage"><a href="home.php">หน้าแรก</a></li>
		<li class="list-hover"><a href="product.php">สินค้า</a></li>
		<li class="list-hover"><a href="order.php">การสั่งซื้อ</a></li>
		<li class="list-hover"><a href="contact.php">ติดต่อเรา</a></li>
	</ul>
</header>
<section>
	<div class="container text-center">

		<div class="row products">

			<div class="col-xs-3 product">
                <a href="{{ URL::to('product\1') }}"><img src="<?php echo asset('assets/img/best-cap/cap1.jpg'); ?>" border="n1"></a>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#oneModal">More Detail</button>
                <div class="modal fade" id="oneModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-xs-3 product">
				<img src="<?php echo asset('assets/img/best-cap/spong1.jpg'); ?>" border="n1" >
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#twoModal">More Detail</button>
                <div class="modal fade" id="twoModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-xs-3 product">
				<img src="<?php echo asset('assets/img/best-cap/hat1.JPG'); ?>" border="n1">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#threeModal">More Detail</button>
                <div class="modal fade" id="threeModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>

		<div class="row products">
			<div class="col-xs-3 product">
				<img src="<?php echo asset('assets/img/best-cap/cap2.jpg'); ?>" border="n1">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sixModal">More Detail</button>
                <div class="modal fade" id="sixModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-xs-3 product">
				<img src="<?php echo asset('assets/img/best-cap/spong2.jpg'); ?>" border="n1">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#fourModal">More Detail</button>
                <div class="modal fade" id="fourModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-xs-3 product">
				<img src="<?php echo asset('assets/img/best-cap/hat2.JPG'); ?>" border="n1">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#fiveModal">More Detail</button>
                <div class="modal fade" id="fiveModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is a large modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<dialog id="detailDialog">
			<div id = "modalDetail">
			</div>
		</dialog>
	</div>
</section>
<footer>
	<div class="row">
		<div class="col-xs-4">
			<p> แนะนำการสั่งซื้อ </p>
			<p> สินค้าทั้งหมด </p>
			<p> เกี่ยวกับเรา </p>
			<p> ติดต่อเรา </p>
			<p> ช่องทางการติดต่อ </p>
			<img class="social" src="<?php echo asset('assets/img/twitter-icon.png'); ?>">
			<img class="social" src="<?php echo asset('assets/img/social-facebook-box-blue-icon.png'); ?>">
			<img class="social" src="<?php echo asset('assets/img/Active-Instagram-3-icon.png'); ?>">

		</div>
		<div class="col-xs-4">

		</div>
		<div class="col-xs-4">

		</div>
	</div>
	<div class="row">
		<div class="text-center">ผลิตโดย born to be cap group สำหรับ CS387 ©2015 มหาวิทยาลัยธรรมศาสตร์</div>
	</div>
</footer>
</body>
</html>
